#!/bin/bash
#Automates the re-imaging process and eliminates time consuming steps
#By Coleman Nugent

#This function prompts the user to press any key
fail () {
	read -rsp $'Press any key to continue...\n' -n1 key
	say "reimage failed"
	exit 1
}

clear
#Inform the user about the necessary permissions
echo "[INFO] This script requires superuser privileges in order to perform privileged operations such as restoring disk images and copying users data. Please enter the superuser password to continue."

#Check if running as root, if not elevate
[ "$UID" -eq 0 ] || exec sudo bash "$0" "$@"
if [ $? -ne 0 ]; then
	echo "[ERROR] Unable to elevate to superuser! Are you logged in as the right account?"
	fail
fi
clear

#Perform various housekeeping duties
source `dirname $0`/modules/housekeeping.sh

#Variables that control various aspects of the script
version="0.2.2"
date_modified="10/22/2015"
image_version="Yosemite 10.10.5"

#Breakdown of the Rsync command:
#Firstly `dirname $0` will evaluate to the directory the script is placed in, not the present working directory
#then we add the path to the Rsync binary. This is done so that we are using a known good version of Rsync on
#any mac. The -a flag means archive and implies several other flags that are designed to preserve all aspects
#of the copied data including permissions and ownership. The -h flag scales the byte totals to the nearest 
#human readable format. --progress shows a progress bar for each file. --no-links instructs Rsync to ignore
#any symbolic links it encounters
rsync=`dirname $0`"/bin/rsync -vaE -h --progress --no-links"

#Note that this will change with each version of our image
image="iMac_Yosemite_avast_keychain_script_10_12_15.i386.hfs.dmg"
image_dir="/Users/hadmin/desktop/images/"
target="/Volumes/Macintosh HD/"

#Print out the version number and info about the script
echo "Coleman's Re-image script v"$version "last modified" $date_modified

#Create a copy of the stdout file descriptor
exec 3>&1

#Get the username
username=$(modules/userselect.sh)

#Backup the user data
source `dirname $0`/modules/backup.sh $username

#re-image the drive
source `dirname $0`/modules/reimage.sh

#Restore the user data
source `dirname $0`/modules/restore.sh $username

#Change the login text
source `dirname $0`/modules/logintext.sh
clear
echo "[INFO] Re-image complete!"
say "reimage complete"
