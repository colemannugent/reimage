# Documentation
#### More in-depth info about this collection
---

## Technical Overview
The `reimage.command` script calls several other helper scripts referred to as "modules":

1. `housekeeping.sh`
	* Checks for internet access
	* Updates the script through `git pull`
2. `userselect.sh`
	* Prompts for the username
3. `backup.sh`
	* Copies user's data to external drive
4. `reimage.sh`
	* Uses the `asr` command to reimage the drive
5. `restore.sh`
	* Copies the user's data back to the internal drive
6. `logintext.sh`
	* Changes the login text to reflect when the drive was reimaged

---

# FAQ's

## Why Bash?
It is available on every iMac that has ever been produced. This combined with including our own Rsync binary ensures that no matter how old the iMac is, the script performs as expected.

## Why the `.command` extension?
This allows the files to be run like any other app without having to run a command in the terminal.

## Does this script eliminate the need for the change permission script?
Actually no. The way UNIX permissions work is that every file has a UID attribute that specifies who the file owner is. When we run the change permission script all it does is change that file attribute on all the files in the user's home directory. However, since the user account is not created until they login using their network credentials, changing the permissions before has no effect since the UID will point to the wrong user.

## How do we change permissions then?
There is an appropriately named `change_ownership.command` that serves the same purpose as the old script but faster and with more detailed output.

## Will we need to update the script with each new image?
Not entirely. The only thing that would need to change is the `image` variable. This variable stores the filename for the image. Currently this value is hardcoded into the script, however there are plans to add a detection system for the images. 

---

# In Detail
The core of the script uses [Rsync](http://linux.die.net/man/1/rsync) to synchronize the two directories. The actual re-imaging process is handled by [Apple Software Restore](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man8/asr.8.html). The rest of the `reimage.command` script serves to check if the specified user's folder is on the system and other various cases where the operator may have erred in their input.
