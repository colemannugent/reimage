#!/bin/bash
#Corrects any permissions errors on the given users files
#By Coleman Nugent

clear
#Inform the user about the nessesary permisssions
echo "[INFO] This script requires superuser priviledges in order to change permissions on the users files. Please enter the superuser password to continue."
clear

#Prompt for the username of the account we want to change the permissions for
echo -n "[PROMPT] Username: "
read username

echo -n "[PROMPT] Superuser password: "
read -s password

#Do literally exactly what the old script did
echo $password | sudo -v -S

#Actually change the ownership of the files
sudo chown -R $username /Users/$username

#Print an error message if the previous command failed
if [ $? -eq 1 ]; then
	clear
	echo "[ERROR] Could not change the permissions for $username!"
	exit 1
fi
clear
echo "[INFO] Finished!"
