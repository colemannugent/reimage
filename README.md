# CSUF Re-Image Script Collection
#### Maintained by Coleman Nugent
---
## Purpose
The current workflow for re-imaging is sub-par. This collection of scripts aims to change that by automating most of the process and eliminating user error. This collection is intended to be an all-in-one solution with dependencies other than Git to maintain the source code.

## What's wrong with the current workflow?
Well, the most obvious thing is that we have no set procedure for re-imaging. We have described the high-level steps in the KB article, but have left out the myriad details that are required to successfully re-image an iMac. This script will solve that by uniting our workflow into one set of scripts.

Additional problems with the current workflow:

1. Data is copied by hand
	* Whoever is doing the re-image has immediate access to the users files
	* Whoever is copying the data must put it in the right location both times
2. OSX copy dialog is not the right tool to copy data
	* It is *very* quiet about what it does in general
	* Resuming a transfer is messy and unreliable
3. Someone has to continually monitor the iMac to make sure it's still going
	* If you start a re-image at the end of the day it will only go so far before it needs someone to perform the next step
	* Telling whether a computer has been re-imaged is very difficult

## Why is this script better?
1. There is no room for human error
2. Data is never seen by the person performing the re-image
3. The data transfer process is based on Rsync, a widely used, open-source reliable file synchronization program
4. Time is saved by using more efficient low-level system utilities
5. This collection can be expanded to have more functionality

## Features
* Robust file synchronization efficiently copies all user data
* Uses native utilities to reduce system overhead
* Modular system supports easy addition of additional features
* Ability to configure various settings such as the login text dynamically

## To Do:
* Log information about the re-image to a server for easy access 
* Automatically update the OSX image using information about the file
* Create a "Bind to domain" script
* Fix issues with keychains
* Add script to hadmin profile on image
* Menu to select user(s) to backup data for
* Set the local sharing name to the AD computer name
* Re-use the computer name from the old HDD
* On first login after re-image alert the user to call helpdesk for setup and show their IP for remote assistance
* Check for dead links on the user's dock

## Documentation
More documentation can be found in the [DOC.md](https://bitbucket.org/colemannugent/reimage/src/master/DOC.md) file.

## Issues
* UID's for users do no persist across computers, therefore we do need to change ownership of the files at the end of the data transfer
* It is a technical impossibility to rebind the computer without booting into the internal drive
