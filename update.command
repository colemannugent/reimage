#!/bin/bash
# This script updates the entire script collection

clear
echo -n "[INFO] Testing network connection..."
curl -s google.com > /dev/null
if [ $? -eq 0 ]; then
	echo " success!"
	echo -n "[INFO] Downloading latest version..."
	cd `dirname $0`
	git pull > /dev/null
	echo " done!"
else
	echo "failed!"
	echo "[ERROR] Unable to connect to the network!"
fi
read -rsp $'Press any key to continue...\n' -n1 key
