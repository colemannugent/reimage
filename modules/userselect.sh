#!/bin/bash
#userselect.sh - Part of the re-image script collection

#Prompt and verify the username
while true; do
	#Prompt for the username of the account we want to backup
	echo -n "[PROMPT] Username: " >&3
	read username

	#Verify username
	echo -n "[PROMPT] Is the username \"$username\" correct? (y/N): " >&3
	read name_confirm

	if [ $name_confirm == "y" ]; then 
		echo $username
		exit
	fi
done
