#!/bin/bash
# housekeeping.sh - Part of the re-image script collection
# Performs various checks and cleanups

#Check for network access
clear
echo -n "[INFO] Testing network connection..."
curl -s google.com > /dev/null
if [ $? -eq 0 ]; then
	echo " success!"
	echo -n "[INFO] Downloading latest version..."
	cd `dirname $0`
	git pull > /dev/null
	echo " done!"
else
	echo "failed!"
	echo "[ERROR] Unable to connect to the network!"
	fail
fi
