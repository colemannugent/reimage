#!/bin/bash
# backup.sh - Part of the re-image script collection
# Backup the specified users data

backup_dir="/Users/hadmin/Desktop/"$1"/"
local_dir="/Volumes/Macintosh HD/Users/"$1"/"

echo "[INFO] Backup directory is "$backup_dir
echo "[INFO] Local directory is "$local_dir

#Copy data to local drive
if [ ! -d "$local_dir" ]; then
	echo "[ERROR] Could not find the user \"$1\" on the local drive!"
	fail
fi
echo "[INFO] Backing up data..."
$rsync "$local_dir" "$backup_dir"
if [ $? -ne 0 ]; then
	clear
	echo "[ERROR] Failed to backup user data!"
	fail
else
	clear 
	echo "[INFO] Successfully backed up user data!"
fi
