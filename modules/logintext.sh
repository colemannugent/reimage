#!/bin/bash
#logintext.sh - Part of the re-image script collection

#Generate login_text variable
login_text="Property of Cal State Fullerton \r\n Information Technology \r\n $image_version $(date +%D)"

#Update login text
sudo defaults write "$target/Library/Preferences/com.apple.loginwindow" LoginwindowText "$login_text"
if [ $? -ne 0 ]; then
	clear
	echo "[ERROR] Unable to change the login text!"
	fail
else
	clear
	echo "[INFO] Successfully changed the login text!"
fi
