#!/bin/bash
# reimage.sh - Part of the re-image script collection
#Verify the image name
while true; do
	echo -n "[PROMPT] Is the image \"$image\" the one we want to restore with (y/N): "
	read image_confirm

	if [ $image_confirm == "y" ]; then
		break
	else
		echo -n "[PROMPT] Enter the filename of the image you want to use: "
		read image
	fi

done

#Re-image the drive with the built in asr command
echo "[INFO] Re-imaging local drive..."
asr --source "$image_dir$image" -t "$target" --erase --noprompt
if [ $? -ne 0 ]; then
	clear
	echo "[ERROR] Re-image failed!"
	fail
else
	clear
	echo "[INFO] Re-image completed successfully!"
fi

