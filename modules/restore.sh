#!/bin/bash
#restore.sh - Part of the re-image script collection
#Copy user data to newly re-imaged drive

backup_dir="/Users/hadmin/Desktop/"$1"/"
local_dir="/Volumes/Macintosh HD/Users/"$1"/"

echo "[INFO] Restoring user data..."
$rsync "$backup_dir" "$local_dir"
if [ $? -ne 0 ]; then
	clear
	echo "[ERROR] Failed to restore user data!"
	fail
else
	clear
	echo "[INFO] Successfully restored user data!"
fi
